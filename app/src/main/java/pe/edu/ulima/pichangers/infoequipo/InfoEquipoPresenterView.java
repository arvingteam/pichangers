package pe.edu.ulima.pichangers.infoequipo;

import pe.edu.ulima.pichangers.beans.Equipo;

/**
 * Created by ENRIQUE on 18/05/2016.
 */
public interface InfoEquipoPresenterView {
    public void setPresenter(InfoEquipoPresenter presenter);
    public void mostrarEquipo(Equipo equipo);
}

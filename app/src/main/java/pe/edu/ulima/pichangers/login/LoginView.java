package pe.edu.ulima.pichangers.login;

/**
 * Created by ENRIQUE on 14/05/2016.
 */
public interface LoginView {
    public void setPresenter(LoginPresenter presenter);
    public void loginCorrecto();
    public void loginIncorrecto();
}

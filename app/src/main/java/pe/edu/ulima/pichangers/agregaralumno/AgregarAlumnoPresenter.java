package pe.edu.ulima.pichangers.agregaralumno;

/**
 * Created by alex on 5/19/16.
 */
public interface AgregarAlumnoPresenter {
    public void obtenerAlumnos(boolean flagEquipo);
    public void addAlumnoEquipo(Long idEquipo, String codigoAlumno);
}

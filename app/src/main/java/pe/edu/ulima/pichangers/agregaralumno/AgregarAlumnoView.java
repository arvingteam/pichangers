package pe.edu.ulima.pichangers.agregaralumno;

import java.util.List;

import pe.edu.ulima.pichangers.beans.Alumno;

/**
 * Created by alex on 5/19/16.
 */
public interface AgregarAlumnoView {
    public void setPresenter(AgregarAlumnoPresenter presenter);
    public void mostrarAlumnos(List<Alumno> alumnos);
    public void notifyAddSuccess(String codAlumno);
}

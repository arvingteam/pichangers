package pe.edu.ulima.pichangers.login;

import pe.edu.ulima.pichangers.beans.LoginUser;

/**
 * Created by ENRIQUE on 13/05/2016.
 */
public interface LoginPresenter {
    public void loginUsuario(LoginUser alumnoUser);
}
